# emdeefive-mojo

Solution for HackTheBox 'Emdee five for life' written using Mojo::UserAgent and GNU core utilities md5sum.

## Requirements
- GNU core utilities
- Mojo::UserAgent
- gpg and challenge flag (to decrypt file)
